#!/bin/bash
set -o allexport

HOME=/config
XDG_CONFIG_HOME=${HOME}/.config
XDG_CACHE_HOME=${HOME}/.cache
XDG_DATA_HOME=${HOME}/.local/share
XDG_RUNTIME_DIR=/tmp
XDG_CONFIG_DIRS=/etc/xdg
WLR_BACKENDS=headless
WLR_LIBINPUT_NO_DEVICES=1
MOZ_ENABLE_WAYLAND=1
MOZ_X11_EGL=1

mkdir -p ${HOME}/{.config/sway,.cache,.local/share}
cp /etc/sway/config ${HOME}/.config/sway/
echo "output * pos 0 0 res ${SCREEN_WIDTH:=1024}x${SCREEN_HEIGTH:=768}" >> ${HOME}/.config/sway/config

/usr/bin/sway &
sleep 1
/usr/bin/firefox-wayland &
/usr/bin/wayvnc 0.0.0.0 &
/usr/share/novnc/utils/launch.sh --listen 8080 --vnc localhost:5900 &

wait

